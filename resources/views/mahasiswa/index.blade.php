

@extends('layouts.app')

@section('content')
<div class="container-fluid content-inner mt-5 py-0">
    <div class="row">
        <div class="col-sm-12">
            <div class="card">
                <div class="card-header d-flex justify-content-between">
                    <div class="header-title">
                        <h4 class="card-title">List Data Mahasiswa</h4>
                    </div>
                </div>
                <div class="card-body">
                    @if (session('success'))
                        <div class="alert alert-success">{{ session('success') }}</div>
                    @endif

                    @if (session('error'))
                        <div class="alert alert-danger">{{ session('error') }}</div>
                    @endif
                    
                    <div class="table-responsive">
                        <table id="example" class="table table-bordered" style="width:100%">
                            <thead>
                            <tr>
                                <th scope="col" class="text-center">No</th>
                                <th scope="col" class="text-center">Nama</th>
                                <th scope="col" class="text-center">NIM</th>
                                <th scope="col" class="text-center">Jurusan</th>
                                <th scope="col" class="text-center">Tanggal Regis</th>
                                <th scope="col" class="text-center">Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($mahasiswas as $mahasiswa)
                                    <tr>
                                        <th scope="row" class="text-center">{{$loop->iteration}}</th>
                                        <td class="text-center">{{ $mahasiswa->nama }}</td>
                                        <td class="text-center">{{ $mahasiswa->nim }}</td>
                                        <td class="text-center">{{ $mahasiswa->jurusan }}</td>
                                        <td class="text-center">{{ $mahasiswa->created_at }}</td>
                                        <td class="text-center">
                                                <div class="btn-group me-2" role="group" aria-label="First group">
                                                    <a href="{{ route('mahasiswa.edit',$mahasiswa->id_mahasiswa) }}"><button type="button" class="btn btn-outline-warning">Edit</button></a>
                                                    <form method="POST" action="{{ route('mahasiswa.destroy', $mahasiswa->id_mahasiswa) }}">
                                                    @method('DELETE')
                                                    @csrf
                                                    <button type="submit" class="btn btn-outline-danger show_confirm">Hapus</button>
                                                    </form>
                                                </div>
                                        </td>
                                    </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('script')
<script type="text/javascript">
    $(document).ready(function() {
        $('#example').append('<caption style="caption-side: bottom">Data List Mahasiswa</caption>');
        $('#example').DataTable({
                dom: 'Bfrtip',
                buttons: [
                    { extend: 'copyHtml5', footer: true },
                    { extend: 'excelHtml5', footer: true },
                    { extend: 'csvHtml5', footer: true }
                ],
            });
        });
</script>
@endsection