@extends('layouts.app')

@section('content')
<div class="container-fluid content-inner mt-5 py-0">
    <div class="row">
        <div class="col-sm-12">
            <div class="card">
                <div class="card-header d-flex justify-content-between">
                    <div class="header-title">
                        <h4 class="card-title">Create Data Mahasiswa</h4>
                    </div>
                </div>
                <div class="card-body">
                    <form method="post" action="{{ route('mahasiswa.update', $mahasiswas->id_mahasiswa) }}" enctype="multipart/form-data">
                        @csrf
                        @method('PUT')
                        @if ($errors->any())
                        <div class="alert alert-danger">
                            <div class="alert-title"><h4>Whoops!</h4></div>
                            There are some problems with your input.
                            <ul>
                                @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div> 
                        @endif

                        @if (session('success'))
                        <div class="alert alert-success">{{ session('success') }}</div>
                        @endif

                        @if (session('error'))
                        <div class="alert alert-danger">{{ session('error') }}</div>
                        @endif


                        <div class="row">
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label for="nama" class="form-label">Nama Mahasiswa</label>
                                    <input type="text" class="form-control" aria-describedby="nama" name="nama" required autocomplete="nama" autofocus value="{{ old('nama', $mahasiswas->nama) }}">
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label for="nim" class="form-label">NIM</label>
                                    <input type="text" class="form-control" aria-describedby="nim" name="nim" required autocomplete="nim" autofocus value="{{ old('nim', $mahasiswas->nim) }}">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label for="jurusan" class="form-label">Jurusan</label>
                                    <input type="text" class="form-control" aria-describedby="jurusan" name="jurusan" required autocomplete="jurusan" autofocus value="{{ old('jurusan', $mahasiswas->jurusan) }}">
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <label for="password" class="form-label">Password</label>
                                <div class="input-group mb-3">
                                    <input type="password" class="form-control" id="password" aria-describedby="password" name="password" required autofocus>
                                    <button id="togglePassword" class="btn btn-outline-secondary" type="button">Show/Hide</button>
                                </div>
                            </div>
                        </div>
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection