@extends('layouts.app')

@section('content')
<div class="container-fluid content-inner mt-5 py-0">
    <div class="row">
        <div class="col-sm-12">
            <div class="card">
                <div class="card-header d-flex justify-content-between">
                    <div class="header-title">
                        <h4 class="card-title">Create Data Mahasiswa</h4>
                    </div>
                </div>
                <div class="card-body">
                    <form method="post" action="{{ route('mahasiswa.store') }}" enctype="multipart/form-data">
                        @csrf
                        @if ($errors->any())
                        <div class="alert alert-danger">
                            <div class="alert-title"><h4>Whoops!</h4></div>
                            There are some problems with your input.
                            <ul>
                                @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div> 
                        @endif

                        @if (session('success'))
                        <div class="alert alert-success">{{ session('success') }}</div>
                        @endif

                        @if (session('error'))
                        <div class="alert alert-danger">{{ session('error') }}</div>
                        @endif

                        <div class="row">
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label for="nama" class="form-label">Nama Mahasiswa</label>
                                    <input type="text" class="form-control" aria-describedby="nama" placeholder="Nama Mahasiwa" name="nama" required autocomplete="nama" autofocus>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label for="nim" class="form-label">NIM</label>
                                    <input type="text" class="form-control" aria-describedby="nim" placeholder="NIM Mahasiswa" name="nim" required autocomplete="nim" autofocus>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label for="jurusan" class="form-label">Jurusan</label>
                                    <input type="text" class="form-control" aria-describedby="jurusan" placeholder="Jurusan Mahasiwa" name="jurusan" required autocomplete="jurusan" autofocus>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <label for="password" class="form-label">Password</label>
                                <div class="input-group mb-3">
                                    <input type="password" class="form-control" id="password" aria-describedby="password" placeholder="Password" name="password" required autofocus>
                                    <button id="togglePassword" class="btn btn-outline-secondary" type="button">Show/Hide</button>
                                </div>
                            </div>
                        </div>
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection