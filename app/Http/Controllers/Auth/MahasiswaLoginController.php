<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\Mahasiswa;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Session;

class MahasiswaLoginController extends Controller
{

    public function login()
    {
        $data['title'] = 'Login Mahasiswa';
        return view('auth.login', $data);
    }

    public function login_action(Request $request)
    {
        $request->validate([
            'nim' => 'required',
            'password' => 'required',
        ]);
        $credentials = $request->only('nim', 'password');
        if (Auth::attempt(['nim' => $request->nim, 'password' => $request->password])) {
            $request->session()->regenerate();
            return redirect()->intended('dashboard');
        }

        return back()->withErrors([
            'password' => 'Wrong NIM or password',
        ]);
    }

    public function logout(Request $request)
    {
        Auth::logout();
        $request->session()->invalidate();
        $request->session()->regenerateToken();
        return redirect('/');
    }
}
