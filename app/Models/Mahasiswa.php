<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;

class Mahasiswa extends Authenticatable
{
    // Sesuaikan dengan nama tabel Anda jika berbeda
    protected $table = 'tb_mahasiswas';
    protected $primaryKey = 'id_mahasiswa';
    protected $fillable = [
        'nama',
        'nim',
        'jurusan',
        'password',
        'created_at',
        'update_at',
    ];
    
}
