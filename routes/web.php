<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Auth\MahasiswaLoginController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\MahasiswaController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::get('mahasiswa/login', [MahasiswaLoginController::class, 'login'])->name('mahasiswa.login');
Route::post('mahasiswa/login', [MahasiswaLoginController::class, 'login_action'])->name('mahasiswa.action.login');
Route::post('logout', [MahasiswaLoginController::class, 'logout'])->name('logout');

Route::middleware('auth')->group(function (){

// HOME
Route::get('dashboard', [HomeController::class, 'index'])->name('dashboard');

// CRUD MAHASISWA
Route::resource('mahasiswa',MahasiswaController::class);


});